# js-hometask-5

## Ответьте на вопросы

1. Какие данные хранятся в следующих свойствах узла: `parentElement`, `children`, `previousElementSibling` и `nextElementSibling`?

> В parentElement хранится родительский тег, в children - дочерние узлы с типом ELEMENT_NODE, в previousElementSibling - предшествующий узел с типом ELEMENT_NODE, в nextElementSibling - следующий узел с типом ELEMENT_NODE.

2. Нарисуй какое получится дерево для следующего HTML-блока.

```html
<p>Hello,<!--MyComment-->World!</p>
```

![Задание 2](./img/htmlStructure.png)

## Выполните задания

1. Клонируй репозиторий;
2. Допиши функции в `tasks.js`;
3. Для проверки работы функций открой index.html в браузере;
4. Создай MR с решением.
