'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock(elemId) {
  document.getElementById(elemId).remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wrappersArray = document.getElementsByClassName('wrapper');
  const sumOfParagraphs = getSumOfParagraphs(wrappersArray);
  removeAllParagraphs(wrappersArray);
  setNewParagraphs(wrappersArray, sumOfParagraphs);

  function getSumOfParagraphs(wrappersArray) {
    let sumArray = [];
    for (const wrapper of wrappersArray) {
      const wrapperChildrenArray = wrapper.children;
      let pSum = 0;
      for (const elem of wrapperChildrenArray) {
        if (elem.localName === 'p') {
          pSum += Number(elem.textContent);
        }
      }
      sumArray.push(pSum)
    }
    return sumArray;
  }
  
  function removeAllParagraphs(wrappersArray) {
    for (const wrapper of wrappersArray) {
      const wrapperChildrenArray = wrapper.children;
      for (let i = wrapperChildrenArray.length-1; i > 0; i--) {
        if (wrapperChildrenArray[i].localName === 'p') {
          wrapperChildrenArray[i].remove();
        }
      }
    }
  }
  
  function setNewParagraphs(wrappersArray, sumOfParagraphs) {
    for (let i = 0; i < wrappersArray.length; i++) {
      const pElem = document.createElement('p');
      pElem.innerText = sumOfParagraphs[i];
      wrappersArray[i].appendChild(pElem);
    }
  }
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
  const inputElem = document.getElementById('changeMe');
  inputElem.setAttribute('value', 'New_value');
  inputElem.setAttribute('type', 'New_type');
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
  const ulElem = document.getElementById('changeChild');
  const liElem1 = document.createElement('li');
  liElem1.innerText = 1;
  ulElem.insertBefore(liElem1, ulElem.children[1]);
  const liElem2 = document.createElement('li');
  liElem2.innerText = 3;
  ulElem.appendChild(liElem2);
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
  const blocksArray = document.getElementById('blocks').children;
  for (const block of blocksArray) {
    if (block.classList.contains('red')) {
      changeColor(block, 'red', 'blue');
    } else {
      changeColor(block, 'blue', 'red');
    }
  }

  function changeColor(block, color1, color2) {
    block.classList.remove(color1);
    block.classList.add(color2);
  }
}

removeBlock('deleteMe');
calcParagraphs();
changeInput();
appendList();
changeColors();
